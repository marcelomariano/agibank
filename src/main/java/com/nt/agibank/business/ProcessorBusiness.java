package com.nt.agibank.business;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;

import com.nt.agibank.interfaces.IProcessorBusiness;
import com.nt.sgibank.model.Client;
import com.nt.sgibank.model.Item;
import com.nt.sgibank.model.Sale;
import com.nt.sgibank.model.Salesman;

public class ProcessorBusiness implements IProcessorBusiness {

	public static ProcessorBusiness getInstance() {
		return new ProcessorBusiness();
	}

	public static void process(File file, final String outPath) throws IOException{
		ProcessorBusiness instance = getInstance();
		instance.salesReportProcessor(file, outPath);
	}

	@Override
	public void salesReportProcessor(File file, String outPath) throws IOException {
		List<String> lines 			= readArtfactLines(file);
		List<Salesman> salesmans 	= generateSalesmansInstance(lines);
		List<Client> clients 		= generateClientInstance(lines);
		List<Sale> sales			= generateSalesInstance(lines, salesmans);
		Sale mostExpensiveSale 		= findMostExpensiveSale(sales);
		Salesman worstSalesman 		= findWorstSalesman(sales);

		generateReport(outPath, clients.size(), salesmans.size(), mostExpensiveSale, worstSalesman);
	}

	@Override
	@SuppressWarnings("deprecation")
	public List<String> readArtfactLines(File file) throws IOException {
		return FileUtils.readLines(file);
	}

	@Override
	public List<Client> generateClientInstance(List<String> lines) {
		List<Client> clients = new ArrayList<>();
		lines.stream().filter(s -> s.startsWith("002"))
		.collect(Collectors.toList()).stream().map(line -> line.split("ç")).forEach(v ->{
			clients.add(Client.build(v[2]).withCnpj(v[1]).withBusinessArea(v[3]));
		});
		return clients;
	}

	@Override
	public List<Salesman> generateSalesmansInstance(List<String> lines) {
		List<Salesman> salesmans = new ArrayList<>();
		lines.stream().filter(s -> s.startsWith("001"))
		.collect(Collectors.toList()).stream().map(line -> line.split("ç")).forEach(v ->{
			salesmans.add(Salesman.build(v[2]).withCpf(v[1]).withSalary(Double.parseDouble(v[3])));
		});
		return salesmans;
	}

	@Override
	public List<Sale> generateSalesInstance(List<String> lines, List<Salesman> salesmans) {
		List<Sale> sales = new ArrayList<>();
		lines.stream().filter(s -> s.startsWith("003"))
		.collect(Collectors.toList()).forEach(s -> {

			Arrays.asList(s).stream().forEach(sale -> {
				List<Item> items = new ArrayList<>();
				String[] sSale = sale.split("ç");
				String sItem = sale.substring(sale.indexOf("["), sale.indexOf("]"))
						.replace("[", "").replace("]", "");
				System.out.println(sItem);
				String[] its = sItem.split(",");
				Arrays.asList(its).forEach(it -> {
					System.out.println(it);
					String[] sI = it.split("-");
					items.add(Item.build(Long.parseLong(sI[0]))
							.withQuantity(Integer.parseInt(sI[1]))
							.withPrice(Double.parseDouble(sI[2])));
				});
				Salesman salesman = salesmans.stream().filter(saleMan -> saleMan.getName().equals(sSale[3])).findFirst().get();
				sales.add(Sale.build(Long.parseLong(sSale[1])).withItem(items).withSalesman(salesman));
			});
			sales.forEach(System.out::println);
		});

		return sales;
	}

	@Override
	public Sale findMostExpensiveSale(List<Sale> sales) {
		Double greatter = Collections.max(sales.stream().map(Sale::getTotal).collect(Collectors.toList()));
		return sales.stream().filter(s -> s.getTotal() == greatter).findFirst().get();
	}

	@Override
	public Salesman findWorstSalesman(List<Sale> sales) {
		Double less = Collections.min(sales.stream().map(Sale::getTotal).collect(Collectors.toList()));
		return sales.stream().filter(s -> s.getTotal() == less).map(Sale::getSalesman).findFirst().get();
	}

	@Override
	public void generateReport(String outPath, int quantityClients, int quantitySalemans,
			Sale mostExpensiveSale, Salesman worstSalesman) throws IOException {

		File file = new File(outPath+"report.done.dat");
		if (!file.exists()) {
			file.createNewFile();
		}
		file.canWrite();
		FileUtils.writeLines(file, 
				Arrays.asList("Quantidade de clientes no arquivo de entrada: ["+quantityClients+"]",
								"Quantidade de vendedor no arquivo de entrada: ["+quantitySalemans+"]",
								"ID da venda mais cara: ["+mostExpensiveSale.getId()+"]",
								"O pior vendedor: ["+worstSalesman.getName()+"]"));

	}


}
