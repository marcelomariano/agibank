package com.nt.agibank;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Optional;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.nt.agibank.business.ProcessorBusiness;

@SpringBootApplication
public class AgibankApplication {

	private static String home = System.getenv("%HOMEPATH%");

	@Value("${in-directory}")
	private static String inPath;

	@Value("${out-directory}")
	private static String outPath;

	public static int TOTAL_FILES = 0;

	public static void main(String[] args) throws Exception {
		SpringApplication.run(AgibankApplication.class, args);

		Optional.ofNullable(inPath = home+inPath).orElse(inPath = "data/in");
		Optional.ofNullable(outPath = home+outPath).orElse(outPath = "data/out");

		WatchService watcher = FileSystems.getDefault().newWatchService();
		Path diretory = Paths.get(inPath);
		diretory.register(watcher, StandardWatchEventKinds.ENTRY_CREATE,
				StandardWatchEventKinds.ENTRY_MODIFY,
				StandardWatchEventKinds.ENTRY_DELETE);
		while (true) {
			WatchKey key = watcher.take();
			Optional<WatchEvent<?>> watchEvent = key.pollEvents().stream().findFirst();
			Path path = (Path) watchEvent.get().context();
			if (FilenameUtils.getExtension(path.toString()).equals("dat")) {
				String realPath = path.toAbsolutePath().toString().replace(path.getFileName().toString(), inPath+File.separator+path.getFileName());

				ProcessorBusiness.process(new File(realPath), outPath);

				System.out.println(path.toString());
			}
			boolean valid = key.reset();
			if (!valid) {
				break;
			}
		}
		watcher.close();

	}

}

