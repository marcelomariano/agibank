package com.nt.agibank.interfaces;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.nt.sgibank.model.Client;
import com.nt.sgibank.model.Sale;
import com.nt.sgibank.model.Salesman;

public interface IProcessorBusiness {

	void salesReportProcessor(File file, String outPath) throws IOException;

	List<String> readArtfactLines(File file) throws IOException;

	List<Client> generateClientInstance(List<String> lines);

	List<Salesman> generateSalesmansInstance(List<String> lines);

	List<Sale> generateSalesInstance(List<String> lines, List<Salesman> salesman);

	Sale findMostExpensiveSale(List<Sale> sales);

	Salesman findWorstSalesman(List<Sale> sales);

	void generateReport(String outPath, int quantityClients, int quantitySalemans,
			Sale mostExpensiveSale, Salesman worstSalesman) throws IOException;

}
