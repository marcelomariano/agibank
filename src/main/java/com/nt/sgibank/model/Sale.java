package com.nt.sgibank.model;
import java.io.Serializable;
import java.util.List;


public class Sale implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private List<Item> items;

	private Salesman salesman;

	public static Sale build(long id){
		Sale sale = new Sale();
		sale.setId(id);
		return sale;
	}

	public Sale withItem(List<Item> items){
		this.items = items;
		return this;
	}

	public Sale withSalesman(Salesman salesman){
		this.salesman = salesman;
		return this;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Salesman getSalesman() {
		return salesman;
	}

	public void setSalesman(Salesman vendedor) {
		this.salesman = vendedor;
	}

	@Override
	public String toString() {
		return "Sale [id=" + id + ", items=" + items + ", salesman=" + salesman
				+ "]";
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public double getTotal() {
		return items.stream().filter(o -> o.getTotalPrice() > 0).mapToDouble(o -> o.getTotalPrice()).sum();
	}


}
