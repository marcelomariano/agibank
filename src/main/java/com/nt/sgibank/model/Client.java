package com.nt.sgibank.model;
import java.io.Serializable;


public class Client implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;

	private String cnpj;

	private String businessArea;

	public static Client build(String name){
		Client client = new Client();
		client.setName(name);
		return client;
	}

	public Client withCnpj(String cnpj){
		this.cnpj = cnpj;
		return this;
	}

	public Client withBusinessArea(String businessArea){
		this.businessArea = businessArea;
		return this;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getBusinessArea() {
		return businessArea;
	}

	public void setBusinessArea(String businessArea) {
		this.businessArea = businessArea;
	}

	@Override
	public String toString() {
		return "Client [name=" + name + ", cnpj=" + cnpj + ", businessArea="
				+ businessArea + "]";
	}

	
}
