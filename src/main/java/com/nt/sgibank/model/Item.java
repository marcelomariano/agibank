package com.nt.sgibank.model;

import java.io.Serializable;

public class Item implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long id;

	private int quantity;

	private double price;

	private double totalPrice;

	public static Item build(long id){
		Item item = new Item();
		item.setId(id);
		return item;
	}

	public Item withQuantity(int quantity){
		this.quantity = quantity;
		return this;
	}

	public Item withPrice(double price){
		this.price = price;
		return this;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getTotalPrice() {
		return this.getQuantity() * this.getPrice();
	}

	@Override
	public String toString() {
		return "Item [id=" + id + ", quantity=" + quantity + ", price=" + price
				+ ", totalPrice=" + getTotalPrice() + "]";
	}

}
