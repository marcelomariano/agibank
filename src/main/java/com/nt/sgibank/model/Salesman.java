package com.nt.sgibank.model;
import java.io.Serializable;


public class Salesman implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String cpf;

	private String name;

	private double salary;

	public static Salesman build(String name){
		Salesman salesman = new Salesman();
		salesman.setName(name);
		return salesman;
	}

	public Salesman withCpf(String cpf){
		this.cpf = cpf;
		return this;
	}

	public Salesman withSalary(double salary){
		this.salary = salary;
		return this;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Salesman [cpf=" + cpf + ", name=" + name + ", salary=" + salary
				+ "]";
	}
	
}
